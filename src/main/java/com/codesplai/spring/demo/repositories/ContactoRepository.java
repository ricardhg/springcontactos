package com.codesplai.spring.demo.repositories;

import com.codesplai.spring.demo.models.Contacto;

import org.springframework.data.repository.CrudRepository;

public interface ContactoRepository extends CrudRepository<Contacto, Long> {
   
}
