package com.codesplai.spring.demo.controllers;

import java.util.Optional;

import com.codesplai.spring.demo.models.Contacto;
import com.codesplai.spring.demo.repositories.ContactoRepository;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path="/contacto")
public class ContactoController {
    
    private final ContactoRepository contactoRepository;

    @Autowired
    public ContactoController(ContactoRepository contactoRepository) {
        this.contactoRepository = contactoRepository;
    }


    @GetMapping("/all")
    public String getAll(Model model){
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto-list";
    }
    

    @GetMapping("/addcontacto")
    public String addContacto(Contacto contacto) {
        return "add-contacto";
    }


     @PostMapping("/addcontacto")
    public String addContacto(@Valid Contacto contacto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-contacto";
        }

        contactoRepository.save(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto-list";
    }
    
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Contacto contacto = contactoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid contacto Id:" + id));
        model.addAttribute("contacto", contacto);
        return "update-contacto";
    }
    
    @PostMapping("/update/{id}")
    public String updateContacto(@PathVariable("id") long id, @Valid Contacto contacto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            contacto.setId(id);
            return "update-contacto";
        }
        
        contactoRepository.save(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto-list";
    }
    
    @GetMapping("/delete/{id}")
    public String deleteContacto(@PathVariable("id") long id, Model model) {
        Contacto contacto = contactoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid contacto Id:" + id));
        contactoRepository.delete(contacto);
        model.addAttribute("contactos", contactoRepository.findAll());
        return "contacto-list";
    }
}